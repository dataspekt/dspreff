# Publish results

# Publish on Gitlab

gert::git_add(".")
gert::git_commit("Automated update")
gert::git_push()

# Publish on Twitter

library("tidyverse")
library("EpiNow2")
Sys.setlocale("LC_ALL","hr_HR.UTF-8")

# (load model summaries)

summarised_estimates <-
    readRDS("model_outputs/national/latest/summarised_estimates.rds")

summary <-
    readRDS("model_outputs/national/latest/summary.rds")

# (extract Reff summary)

reff <-
    summarised_estimates %>%
    filter(variable %in% "R")

# (date of estimate)

date_of_estimate <-
    reff %>%
    filter(type %in% "estimate based on partial data") %>%
    pull(date) %>%
    max()

# (extract current estimate)

reff_current <-
    reff %>% filter(date == date_of_estimate)

# (verbose estimates)

verb_reff <-
    paste0(formatC(reff_current$median, digits = 1, format = "f"), " [90% CrI ", formatC(reff_current$lower_90, digits = 1, format = "f"), ", ", formatC(reff_current$upper_90, digits = 1, format = "f"),"]")

verb_doubling_time <-
    paste0(formatC(summary$numeric_estimate[[5]]$median, digits = 1, format = "f"), " [90% CrI ", formatC(summary$numeric_estimate[[5]]$lower_90, digits = 1, format = "f"), ", ", formatC(summary$numeric_estimate[[5]]$upper_90, digits = 1, format ="f"),"] dana")

verb_cases_by_infection_date <-
    paste0(round(summary$numeric_estimate[[1]]$median,1), " [90% CrI ", round(summary$numeric_estimate[[1]]$lower_90,1), ", ", round(summary$numeric_estimate[[1]]$upper_90,1),"].")

# (compose tweet text)

tweet_text <-
    paste0("Srednja vrijednost procjene #covid19hr efektivnog reprodukcijskog broja na dan ", trimws(strftime(date_of_estimate, "%e. %B %Y.")), " iznosi ", verb_reff, ".")

# (get image url)

image_url <-
    list.files("model_outputs/national/figures", full.names = TRUE) %>%
    last()

# (post tweet)

rtweet::post_tweet(
    status = tweet_text,
    media = image_url
)
