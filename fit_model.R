library(tidyverse)
library(EpiNow2)

# Read data

data <-
    readRDS("data/dataset_merged.rds") %>%
    slice_tail(n = 180) # (select last 180 days)

# Set model parameters

# (reporting delay)

reporting_delay <-
    estimate_delay(
        rlnorm(1000,  log(3), 1),
        max_value = 15,
        bootstraps = 1
    )

# (generation time)

generation_time <-
    get_generation_time(
        disease = "SARS-CoV-2",
        source = "ganyani"
    )

# (incubation period)

incubation_period <-
    get_incubation_period(
        disease = "SARS-CoV-2",
        source = "lauer"
    )

# Fit model 

estimates <-
    epinow(
        reported_cases = data,
        generation_time = generation_time,
        delays = delay_opts(incubation_period, reporting_delay),
        rt = rt_opts(prior = list(mean = 1, sd = .4)),
        #rt = NULL, backcalc = backcalc_opts(),
        #obs = obs_opts(scale = list(mean = 0.4, sd = 0.05)),
        stan = stan_opts(samples = 2000, chains = 4, cores = 4),
        output = c("latest", "timing"),
        target_folder = "model_outputs/national",
        logs = "model_outputs/logs",
        verbose = TRUE
    )

