library(tidyverse)

# Check max hdi range

max_hdi_range <-
    readRDS("model_outputs/national/latest/summarised_estimates.rds") %>%
    filter(variable %in% "R") %>%
    filter(type %in% "estimate") %>%
    mutate(hdi_range = upper_90 - lower_90) %>%
    pull(hdi_range) %>% max()

# Save validation results

validation <-
    tribble(
        ~test, ~valid,
        "max_hdi_range", if_else(max_hdi_range > .5, FALSE, TRUE)
    )

saveRDS(validation, "data/validation.rds")

